var rect = require("./rectangle");

function solveRect(l, b) {
  console.log("solving l:" + l + " b:" + b);

  rect(l, b, (err, rectangle) => {
    if (err) {
      console.log("error: " + err.message);
    } else {
      console.log(
        "The area of the rect of l:" +
          l +
          " and b:" +
          b +
          " is " +
          rectangle.area()
      );
      console.log(
        "The perimeter of the rect of l:" +
          l +
          " and b:" +
          b +
          " is " +
          rectangle.perimeter()
      );
    }
  });

  console.log("this statement is after calling rect");
}

solveRect(2, 4);
solveRect(3, 5);
solveRect(0, 5);
solveRect(3, 0);
solveRect(-3, 5);
